//
//  DirectoryNNRTests.swift
//  DirectoryNNRTests
//
//  Created by sasha on 1/15/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import XCTest
@testable import DirectoryNNR


class DirectoryNNRTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFetchDepartments() {
        API.FetchDepartments { (response, error) in
            XCTAssertNil(error)
            XCTAssertNil(response)
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
