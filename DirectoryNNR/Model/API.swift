//
//  API.swift
//  DirectoryNNR
//
//  Created by sasha on 12/17/17.
//  Copyright © 2017 sasha. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class API {

    static func SignIn(user: String, pass: String, regID: String = "", completionHandler:@escaping (User?, Error?) -> Void) {
        Alamofire.request((SIGNIN + "?user=\(user)&pass=\(pass)&regID=\(regID)").addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!,
                         method: .post)
            .responseObject {(response: DataResponse<User>) in
                switch response.result {
                case .success:
                    completionHandler(response.result.value, nil)
                case .failure(let error):
                    completionHandler(nil, error)
                }
        }
 
    }
    
    static func SignUP(user: String, pass: String, regID: String = "", completionHandler:@escaping (User?, Error?) -> Void) {
        Alamofire.request((SIGNUP + "?user=\(user)&pass=\(pass)&regID=\(regID)").addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!,
            method: .post)
            .responseObject {(response: DataResponse<User>) in
                switch response.result {
                case .success:
                    completionHandler(response.result.value, nil)
                case .failure(let error):
                    completionHandler(nil, error)
                }
        }
        
    }
    
    static func GetRoot(completionHandler:@escaping ([Item]?, Error?) -> Void) {
        Alamofire.request((GETROOT).addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!,
                          method: .post)
            .responseArray {(response: DataResponse<[Item]>) in
                switch response.result {
                case .success:
                    completionHandler(response.result.value, nil)
                case .failure(let error):
                    completionHandler(nil, error)
                }
        }
        
    }
    
    static func GetFolder(id: Int, completionHandler:@escaping ([Item]?, Error?) -> Void) {
        Alamofire.request((GETFOLDER + "?id=\(id)").addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!,
                          method: .post)
            .responseArray {(response: DataResponse<[Item]>) in
                switch response.result {
                case .success:
                    completionHandler(response.result.value, nil)
                case .failure(let error):
                    completionHandler(nil, error)
                }
        }
        
    }
    
    
    static func RefreshTOKEN(user_id: Int, regID: String, completionHandler: @escaping (Result?, Error?) -> Void) {
        Alamofire.request((REFRESH_TOKEN + "?user_id=\(user_id)&regID=\(regID)").addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!,
                          method: .post)
            .responseObject {(response: DataResponse<Result>) in
                switch response.result {
                case .success:
                    completionHandler(response.result.value, nil)
                case .failure(let error):
                    completionHandler(nil, error)
                }
        }
    }
    
    static func ClearTOKEN(user_id: Int, completionHandler: @escaping (Result?, Error?) -> Void) {
        Alamofire.request((CLEAR_TOKEN + "?user_id=\(user_id)").addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!,
                          method: .post)
            .responseObject {(response: DataResponse<Result>) in
                switch response.result {
                case .success:
                    completionHandler(response.result.value, nil)
                case .failure(let error):
                    completionHandler(nil, error)
                }
        }
    }
    
    static func FindByRoot(name: String, completionHandler:@escaping ([Item]?, Error?) -> Void) {
        Alamofire.request((FINDBYROOT + "?name=\(name)").addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!,
                          method: .post)
            .responseArray {(response: DataResponse<[Item]>) in
                switch response.result {
                case .success:
                    completionHandler(response.result.value, nil)
                case .failure(let error):
                    completionHandler(nil, error)
                }
        }
        
    }
    
    
    static func FindByFolder(id: Int, name: String, completionHandler:@escaping ([Item]?, Error?) -> Void) {
        Alamofire.request((FINDBYFOLDER + "?id=\(id)&name=\(name)").addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!,
                          method: .post)
            .responseArray {(response: DataResponse<[Item]>) in
                switch response.result {
                case .success:
                    completionHandler(response.result.value, nil)
                case .failure(let error):
                    completionHandler(nil, error)
                }
        }
        
    }
    
    
}
