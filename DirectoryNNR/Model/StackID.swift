//
//  StackID.swift
//  DirectoryNNR
//
//  Created by sasha on 3/28/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import Foundation

struct StackID<Int> {
    fileprivate var array = [Int]()
    
    mutating func push(_ element: Int) {
        array.append(element)
    }
    
    mutating func pop() -> Int {
        if array.isEmpty {
            return 0 as! Int
        }
        return array.popLast()!
    }
    
    func peek() -> Int {
        if array.last == nil {
            return 0 as! Int
        }
        return array.last!
    }
    
    func counts() -> Int {
        return array.count as! Int
    }
    
    mutating func clear() {
        self.array = []
    }
}
