//
//  DocumentsController.swift
//  DirectoryNNR
//
//  Created by sasha on 12/16/17.
//  Copyright © 2017 sasha. All rights reserved.
//

import UIKit
import JSQWebViewController
import Popover
import PKHUD
import LPSnackbar

class DocumentsController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var stackLists = Stack<Item>()
    var stackID = StackID<Int>()
    
    var isBackExist: Bool = false
    
    var filtered:Array<Item>?
    let myNotificationKey = "com.notificationKey"
    var items: [Item]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        API.GetRoot { (response, error) in
            if (error == nil) {
                self.items = response
                self.tableView.reloadData()
                HUD.hide()
            } else {
                HUD.show(.labeledError(title: "", subtitle: "Нет интернет соединения проверьте подключение!"))
                HUD.hide(afterDelay: 1, completion: { (error) in
                    self.navigationController?.popToRootViewController(animated: true)
                    return
                })
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(receiveNotification(notification:)), name: NSNotification.Name(rawValue: "Notification"), object: nil)
   /* NotificationCenter.default.addObserver(self, selector: #selector(notificationRecevied(notification:)), name: NSNotification.Name(rawValue: "newDataToLoad"), object: nil)*/
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
       
    }
    
    
    @IBAction func logoutButton_Pressed(_ sender: UIButton) {
        API.ClearTOKEN(user_id: User.sharedInstanse.id!) { (response, error) in
            
        }
        
        User.saveUserDefaults(isLogin: false)
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    
    @objc func receiveNotification(notification: Notification) {
        print("NOTIFICATIONS!!!!!!!!!!!!!!!!!")
        if let data = notification.object as? [String : Any] {
            print(data["notification"]!)
            let snack = LPSnackbar(title: data["notification"]! as! String, buttonTitle: "Обновить")
            // Customize the snack
            snack.bottomSpacing = (tabBarController?.tabBar.frame.height ?? 0) + 5
            snack.height = 55
            snack.view.titleLabel.font = UIFont.systemFont(ofSize: 14)
            
            snack.show(displayDuration: 60, animated: true) { (button) in
                self.stackLists.clear()
                self.stackID.clear()
                HUD.show(.progress)
                
                API.GetRoot { (response, error) in
                    if (error == nil) {
                        self.items = response
                        self.tableView.reloadData()
                        HUD.hide()
                    } else {
                        HUD.show(.labeledError(title: "", subtitle: "Нет интернет соединения проверьте подключение!"))
                        HUD.hide(afterDelay: 1, completion: { (error) in
                            return
                        })
                    }
                }
            }
        }
    }
    
// MARK: - UI SEARCH BAR DELEGATE
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        self.searchBar.showsCancelButton = false
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        self.searchBar.text = ""
        self.tableView.reloadData()
        self.searchBar.showsCancelButton = false
    }
    
    
// MARK: - TABLEVIEW DELEGATE
    
    func scrollToFirstRow() {
        let indexPath = IndexPath(row: 0, section: 0)
        self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Documents table
        if items?.count == nil {
            return 0
        } else {
            if isFiltering() {
                if filtered != nil{
                    return (filtered?.count)!
                } else {
                    return 0
                }
            }
            return items!.count
        }
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Back cell
        if indexPath.row == 0 && Item.isBackExists(items: self.items!) && !isFiltering(){
            let cell = tableView.dequeueReusableCell(withIdentifier: "BackCell") as! BackCell
            return cell
        }
       
        // Folder cell
        var isFold = false
        if self.isFiltering() {
            isFold = self.filtered![indexPath.row].isFolder!
        } else {
            isFold = self.items![indexPath.row].isFolder!
        }
        
        if isFold {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FolderCell") as! FolderCell
            if self.isFiltering() {
                if indexPath.row <= (filtered?.count)! {
                    cell.configureCell(item: filtered![indexPath.row])
                } else {
                    cell.configureCell(item: self.items![indexPath.row])
                }
            } else {
                cell.configureCell(item: self.items![indexPath.row])
            }
            return cell
        }
        // File cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "FileCell") as! FileCell
        if self.isFiltering() {
            if indexPath.row <= (filtered?.count)! {
                cell.configureCell(item: filtered![indexPath.row])
            } else {
                cell.configureCell(item: self.items![indexPath.row])
            }
        } else {
            cell.configureCell(item: self.items![indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Back to parent
        if (indexPath.row == 0) && (Item.isBackExists(items: self.items!)) && (self.stackLists.counts() > 0) && !isFiltering(){
            self.items = self.stackLists.pop()
            self.stackID.pop()
            self.tableView.reloadData()
            self.scrollToFirstRow()

           
        } else {
            var isFold = false
            if self.isFiltering() {
                isFold = self.filtered![indexPath.row].isFolder!
            } else {
                isFold = self.items![indexPath.row].isFolder!
            }
            
            if isFold {
                HUD.show(.progress)
                
                var id: Int?
                if self.isFiltering() {
                    id = self.filtered![indexPath.row].id!
                    self.searchBar.text = ""
                    self.searchBar.resignFirstResponder()
                } else {
                    id = self.items![indexPath.row].id!
                }
                
               API.GetFolder(id: id!, completionHandler: { (response, error) in
                    if error == nil {
                    // List to stack
                        self.stackLists.push(self.items!)
                        self.stackID.push(id!)
                        // new list from API
                        self.items = response
                        // if exist back
                        if Item.isBackExists(items: self.items!) {
                            self.items?.insert(Item(), at: 0)
                        }
                
                        self.tableView.reloadData()
                        self.scrollToFirstRow()
                        HUD.hide()
                    } else {
                        HUD.show(.labeledError(title: "", subtitle: "Нет интернет соединения проверьте подключение!"))
                        HUD.hide(afterDelay: 1, completion: { (error) in
                            return
                        })
                    }
                })
            } else {
                // if file open url
                let url: URL?
                if isFiltering() {
                    url = URL(string: filtered![indexPath.row].url!.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)
                } else {
                    url = URL(string: (self.items![indexPath.row].url?.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)!)
                }
            
                let controller = WebViewController(url: url!)
                let nav = UINavigationController(rootViewController: controller)
                present(nav, animated: true, completion: nil)
            }
        }
    }
//MARK: - Search
    
    func isFiltering() -> Bool {
        if searchBar.text! == "" {
            return false
        } else {
            return true
        }
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchBar.showsCancelButton = true
        /*filtered = items?.filter({( itm : Item) -> Bool in
            return (itm.name?.lowercased().contains(searchText.lowercased()))!
        })*/
        if stackID.peek() == 0 {
            API.FindByRoot(name: searchText.lowercased()) { (response, error) in
                if error == nil {
                    self.filtered = response
                    self.tableView.reloadData()
                } else {
                    HUD.show(.labeledError(title: "", subtitle: "Ошибка!"))
                    HUD.hide(afterDelay: 1, completion: { (error) in
                        
                        return
                    })
                }
            }
        } else {
            API.FindByFolder(id: stackID.peek(),name: searchText.lowercased()) { (response, error) in
                if error == nil {
                    self.filtered = response
                    self.tableView.reloadData()
                } else {
                    HUD.show(.labeledError(title: "", subtitle: "Ошибка!"))
                    HUD.hide(afterDelay: 1, completion: { (error) in
                        
                        return
                    })
                }
            }
        }
        self.filtered = nil
        self.tableView.reloadData()
    }
    
//MARK:  - CHANGE ORIENTATION

    override open var shouldAutorotate: Bool {
        return true
    }
}
