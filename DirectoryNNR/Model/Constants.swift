//
//  Constants.swift
//  DirectoryNNR
//
//  Created by sasha on 12/17/17.
//  Copyright © 2017 sasha. All rights reserved.
//

import Foundation


    let HOST = "http://zyrsas.pythonanywhere.com/"
    // let HOST = "http://northkristofer.pythonanywhere.com/"
    let SIGNIN = HOST + "sign_in/"
    let SIGNUP = HOST + "sign_up/"
    let GETROOT = HOST + "get_root/"
    let GETFOLDER = HOST + "get_folder/"
    let FINDBYROOT = HOST + "find_root/"
    let FINDBYFOLDER = HOST + "find_folder/"
    let REFRESH_TOKEN = HOST + "refresh_token/"
    let CLEAR_TOKEN = HOST + "clear_token/"
    let LICENCE = HOST + "licence/"


    var REGID = ""
