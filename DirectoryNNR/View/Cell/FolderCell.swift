//
//  FolderCell.swift
//  DirectoryNNR
//
//  Created by sasha on 3/6/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import UIKit

class FolderCell: UITableViewCell {

    @IBOutlet weak var nameFolder: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configureCell(item: Item) {
        nameFolder.text = item.name!
        descriptionLabel.text = "(папок \(item.children_count!), файлов \(item.file_count!))"

    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
