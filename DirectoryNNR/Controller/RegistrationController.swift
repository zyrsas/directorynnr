//
//  RegistrationController.swift
//  DirectoryNNR
//
//  Created by sasha on 1/6/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import PKHUD
import JSQWebViewController

class RegistrationController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var loginField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var PassField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var RepeatField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var LicenceSwitch: UISwitch!
    @IBOutlet weak var licenceLabel: UILabel!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTextField()
        
        // Licence Label touch
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapLisense))
        licenceLabel.addGestureRecognizer(tap)
    }

    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.backgroundColor = ColorHelper.hexStringToUIColor(hex: "33495E")
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = ColorHelper.hexStringToUIColor(hex: "33495E")
        self.title = "Регистрация"
        //self.navigationController?.navigationBar.backItem?.title = " "
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
//MARK: - SHOW LICENCE
    
    @objc func tapLisense() {
        let url = URL(string: LICENCE)
        let controller = WebViewController(url: url!)
        
        controller.displaysWebViewTitle = true
        
        let nav = UINavigationController(rootViewController: controller)
        
        present(nav, animated: true, completion: nil)
    }
    


//MARK: - Button
    
    @IBAction func RegistrButton_Pressed(_ sender: UIButton) {
        if ((loginField.text == "") || (PassField.text == "") || (RepeatField.text == "")) {
            HUD.show(.labeledError(title: "", subtitle: "Заполните все поля!"))
            HUD.hide(afterDelay: 2, completion: { (error) in
                return
            })
            return
        }
        
        if PassField.text != RepeatField.text {
            HUD.show(.labeledError(title: "", subtitle: "Пароли не совпадают!"))
            HUD.hide(afterDelay: 2, completion: { (error) in
                return
            })
            return
        }
        
        if !LicenceSwitch.isOn {
            HUD.show(.labeledError(title: "", subtitle: "Вы не прийняли лицензионное соглашения"))
            HUD.hide(afterDelay: 2, completion: { (error) in
                return
            })
            return
        }
        
        
        API.SignUP(user: loginField.text!, pass: PassField.text!, regID: REGID ) { response, error  in
            if (error == nil && response?.id != nil) {
                User.sharedInstanse = User(id: (response?.id)!, name: (response?.name)!, pass: self.PassField.text!, access: (response?.access)!)
                
                User.saveUserDefaults(isLogin: true)
                
                HUD.show(.success)
                HUD.hide(afterDelay: 1, completion: { (error) in
                    if !User.sharedInstanse.access! {
                        self.performSegue(withIdentifier: "accessDenied", sender: nil)
                    } else {
                        HUD.show(.progress)
                        self.performSegue(withIdentifier: "showDOC", sender: nil)
                    }
                })
                
            } else {
                HUD.show(.labeledError(title: "", subtitle: "Пользователь с таким именем уже существует!"))
                HUD.hide(afterDelay: 2, completion: { (error) in
                    return
                })
                return
            }
        }
    }
    
// MARK: - Configure Text Field
    
    func configureTextField() {
        loginField.delegate = self
        loginField.iconFont = UIFont(name: "FontAwesome", size: 17)
        loginField.iconText = "\u{f007}"
        
        PassField.delegate = self
        PassField.iconFont = UIFont(name: "FontAwesome", size: 17)
        PassField.iconText = "\u{f023}"
        
        RepeatField.delegate = self
        RepeatField.iconFont = UIFont(name: "FontAwesome", size: 17)
        RepeatField.iconText = "\u{f023}"
    }
    
//MARK: - Orientation
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .portrait
        }
        return .all
    }
    
// MARK: - Keyboard
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        self.view.endEditing(true)
    }
    
    
}
