//
//  AccessController.swift
//  DirectoryNNR
//
//  Created by sasha on 1/10/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import UIKit
import PKHUD

class AccessController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        HUD.hide()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @IBAction func logOut_Pressed(_ sender: UIButton) {
        API.ClearTOKEN(user_id: User.sharedInstanse.id!) { (response, error) in
           
        }
        
        User.saveUserDefaults(isLogin: false)
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    override open var shouldAutorotate: Bool {
        if UIDevice.current.userInterfaceIdiom == .phone {
            if UIDevice.current.orientation.isPortrait {
                return true
            }
            return false
        } else {
            return true
        }
    }

}
