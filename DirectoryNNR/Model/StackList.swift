//
//  StackList.swift
//  DirectoryNNR
//
//  Created by sasha on 3/6/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import Foundation


struct Stack<Item> {
    fileprivate var array = [[Item]]()
    
    mutating func push(_ element: [Item]) {
        array.append(element)
    }
    
    mutating func pop() -> [Item] {
        if array.isEmpty {
            return []
        }
        return array.popLast()!
    }
    
    func peek() -> [Item] {
        return array.last!
    }
    
    func counts() -> Int {
        return array.count
    }
    
    mutating func clear() {
        self.array = []
    }
}
