//
//  CheckInternet.swift
//  DirectoryNNR
//
//  Created by sasha on 1/7/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import Foundation
import Alamofire
class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
