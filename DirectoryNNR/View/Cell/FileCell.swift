//
//  FileCell.swift
//  DirectoryNNR
//
//  Created by sasha on 12/16/17.
//  Copyright © 2017 sasha. All rights reserved.
//

import UIKit


class FileCell: UITableViewCell {

    @IBOutlet weak var extensionImage: UIImageView!
    @IBOutlet weak var nameFile: UILabel!
    @IBOutlet weak var dateFile: UILabel!
    @IBOutlet weak var sizeFile: UILabel!
    @IBOutlet weak var progresView: UIProgressView!
    @IBOutlet weak var downloadButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(item: Item) {
        nameFile.text = item.name!
        dateFile.text = item.date!
        sizeFile.text = item.size!
        
        configImageExtension(ext: item.ext!)
    }
    
    func configImageExtension(ext: String) {
        switch ext {
        case "jpg":
            extensionImage.image = UIImage(named: "jpg")
        case "jpeg":
            extensionImage.image = UIImage(named: "jpeg")
        case "doc":
            extensionImage.image = UIImage(named: "doc")
        case "docx":
            extensionImage.image = UIImage(named: "docx")
        case "mp3":
            extensionImage.image = UIImage(named: "mp3")
        case "html":
            extensionImage.image = UIImage(named: "html")
        case "txt":
            extensionImage.image = UIImage(named: "txt")
        case "pdf":
            extensionImage.image = UIImage(named: "pdf")
        case "png":
            extensionImage.image = UIImage(named: "png")
        case "pptx":
            extensionImage.image = UIImage(named: "ppt")
        case "ppt":
            extensionImage.image = UIImage(named: "ppt")
        case "xls":
            extensionImage.image = UIImage(named: "xls")
        case "xlsx":
            extensionImage.image = UIImage(named: "xls")
        default:
            extensionImage.image = UIImage(named: "unknown")
        }
    }
    
}
