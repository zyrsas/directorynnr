//
//  Item.swift
//  DirectoryNNR
//
//  Created by sasha on 3/5/18.
//  Copyright © 2018 sasha. All rights reserved.
//


import Foundation
import ObjectMapper


class Item: NSObject, Mappable{
    var id: Int?
    var file_count: Int?
    var parent_id: Int?
    var ext: String?
    var name: String?
    var size: String?
    var children_count: Int?
    var isFolder: Bool?
    var date: String?
    var url: String?
    
    init(id: Int, file_count: Int, parent_id: Int, ext: String, name: String, size: String, children_count: Int, isFolder: Bool, date: String, url: String){
        self.id = id
        self.file_count = file_count
        self.parent_id = parent_id
        self.ext = ext
        self.name = name
        self.size = size
        self.children_count = children_count
        self.isFolder = isFolder
        self.date = date
        self.url = url
    }
    
    override init() {
        self.id = -1
        self.file_count = -1
        self.parent_id = -1
        self.ext = ""
        self.name = ""
        self.size = ""
        self.children_count = 0
        self.isFolder = true
        self.date = ""
        self.url = ""
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.file_count <- map["file_count"]
        self.parent_id <- map["parent_id"]
        self.ext <- map["extension"]
        self.name <- map["name"]
        self.size <- map["size"]
        self.children_count <- map["children_count"]
        self.isFolder <- map["isFolder"]
        self.date <- map["date"]
        self.url <- map["url"]
    }
    
    static func isBackExists(items: [Item]) -> Bool {
        if items.isEmpty {
            return true
        }
        
        for item in items {
            if (item.parent_id != nil) {
                return true
            }
        }
        return false
    }
}

