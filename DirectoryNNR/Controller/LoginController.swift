//
//  ViewController.swift
//  DirectoryNNR
//
//  Created by sasha on 12/15/17.
//  Copyright © 2017 sasha. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import PKHUD

class LoginController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var loginField: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var PassField: SkyFloatingLabelTextFieldWithIcon!
    
    @IBOutlet weak var constForKeyboard: NSLayoutConstraint!

    static var constCent: CGFloat?
    var heightStack: CGFloat?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if User.checkUserLogin() == nil {
           
        } else {
            if User.checkUserLogin()! {
                HUD.show(.progress)
                
                User.loadUserDefaults()
                
                let defaults = UserDefaults.standard
                let TOKEN = defaults.string(forKey: "firebase_token")
            
                API.SignIn(user: User.sharedInstanse.name!, pass: User.sharedInstanse.pass!, regID: TOKEN! ) { response, error  in
                    if (error == nil && response?.id != nil) {
                        User.sharedInstanse = User(id: (response?.id)!, name: (response?.name)!, pass: User.sharedInstanse.pass!,  access: (response?.access)!)
                        
                        User.saveUserDefaults(isLogin: true)
                        
                        if !User.sharedInstanse.access! {
                            self.performSegue(withIdentifier: "accessDeniedLogin", sender: nil)
                        } else {
                            self.performSegue(withIdentifier: "showDocuments", sender: nil)
                        }
                    } else {
                        HUD.hide()
                    }
                }
            }
            if User.checkUserLogin() == false {
               
            }
        }
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            heightStack = 470
        } else {
            heightStack = 320
        }
        
        configureTextField()

        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
       
        if LoginController.constCent == nil {
            LoginController.constCent = (UIScreen.main.bounds.height / 2) - (self.heightStack! / 2) - 50
        }
        
        constForKeyboard.constant = LoginController.constCent!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
// MARK: - Configure Text Field
    
    func configureTextField() {
        loginField.delegate = self
        loginField.iconFont = UIFont(name: "FontAwesome", size: 17)
        loginField.iconText = "\u{f007}"
        
        PassField.delegate = self
        PassField.iconFont = UIFont(name: "FontAwesome", size: 17)
        PassField.iconText = "\u{f023}"
    }
    
// MARK: - Keyboard
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
   
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        self.view.endEditing(true)
    }
    
// MARK: - Button Login
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        if (loginField.text != "") && (PassField.text != "")  {
            if !Connectivity.isConnectedToInternet {
                HUD.show(.labeledError(title: "", subtitle: "Нет интернет соединения проверьте подключение!"))
                HUD.hide(afterDelay: 1, completion: { (error) in
                    self.navigationController?.popToRootViewController(animated: true)
                    return
                })
                return
            }
            HUD.show(.progress)
            
            API.SignIn(user: loginField.text!, pass: PassField.text!, regID: REGID ) { response, error  in
                if (error == nil && response?.id != nil) {
                    User.sharedInstanse = User(id: (response?.id)!, name: (response?.name)!, pass: self.PassField.text!, access: (response?.access)!)
                    
                    User.saveUserDefaults(isLogin: true)
                    
                    if !User.sharedInstanse.access! {
                        self.performSegue(withIdentifier: "accessDeniedLogin", sender: nil)
                    } else {
                        self.performSegue(withIdentifier: "showDocuments", sender: nil)
                    }
                } else {
                    HUD.hide()
                    self.loginField.errorMessage = "Пользователь не существует"
                    self.PassField.errorMessage = "      "
                }
            }

        }
    }

    
//MARK: - TextField Delegate, delete error message
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            if(loginField.errorMessage != "" ){
                loginField.errorMessage = ""
                PassField.errorMessage = ""
            }
        return true
    }
    
//MARK: - Keyboard
    
    @objc func keyBoardWillShow(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        let check = UIScreen.main.bounds.height - (LoginController.constCent! + self.heightStack!)
  
        if keyboardFrame.height > check {
            constForKeyboard.constant = LoginController.constCent! - (keyboardFrame.height - check )
        }
    }
    
    @objc func keyBoardWillHide(notification: NSNotification) {
        constForKeyboard.constant = LoginController.constCent!
    }

    override open var shouldAutorotate: Bool {
        if UIDevice.current.userInterfaceIdiom == .phone {
             if UIDevice.current.orientation.isPortrait {
                return true
            }
            return false
        } else {
            return true
        }
    }
    
    
}

extension UINavigationController {
    
    override open var shouldAutorotate: Bool {
        get {
            if let visibleVC = visibleViewController {
                return visibleVC.shouldAutorotate
            }
            return super.shouldAutorotate
        }
    }
    
    override open var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation{
        get {
            if let visibleVC = visibleViewController {
                return visibleVC.preferredInterfaceOrientationForPresentation
            }
            return super.preferredInterfaceOrientationForPresentation
        }
    }
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        get {
            if let visibleVC = visibleViewController {
                return visibleVC.supportedInterfaceOrientations
            }
            return .portrait
        }
    }}
