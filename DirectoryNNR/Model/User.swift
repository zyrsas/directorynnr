//
//  User.swift
//  DirectoryNNR
//
//  Created by sasha on 12/17/17.
//  Copyright © 2017 sasha. All rights reserved.
//

import Foundation
import ObjectMapper

class User: NSObject, Mappable{
    var id: Int?
    var name: String?
    var pass: String?
    var access: Bool?
    
    static var sharedInstanse = User()
    
    init(id: Int, name: String, pass: String, access: Bool){
        self.id = id
        self.name = name
        self.pass = pass
        self.access = access
    }
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        access <- map["access"]
    }
    
    static func saveUserDefaults(isLogin: Bool) {
        let defaults = UserDefaults.standard
        defaults.set(User.sharedInstanse.id, forKey: "id")
        defaults.set(User.sharedInstanse.name, forKey: "name")
        defaults.set(User.sharedInstanse.access, forKey: "access")
        defaults.set(User.sharedInstanse.pass, forKey: "pass")
        
        defaults.set(isLogin, forKey: "isLogin")
    }
    
    static func loadUserDefaults() {
        let defaults = UserDefaults.standard
        User.sharedInstanse.id = defaults.integer(forKey: "id")
        User.sharedInstanse.name = defaults.string(forKey: "name")
        User.sharedInstanse.access = defaults.bool(forKey: "access")
        User.sharedInstanse.pass = defaults.string(forKey: "pass")
    }
    
    static func checkUserLogin() -> Bool? {
        let defaults = UserDefaults.standard
        return defaults.bool(forKey: "isLogin")
    }
    
    
    
}
