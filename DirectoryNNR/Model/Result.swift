//
//  Department.swift
//  DirectoryNNR
//
//  Created by sasha on 1/7/18.
//  Copyright © 2018 sasha. All rights reserved.
//

import Foundation
import ObjectMapper

class Result: NSObject, Mappable{
    var result: Bool?

    init(result: Bool){
        self.result = result
    }
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        result <- map["result"]
    }
}

